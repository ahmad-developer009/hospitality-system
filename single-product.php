<?php require_once('header.php'); ?>



<!------------ Rehan : Simple Page START--------------------------------->
<section class="section-padding page">
  <div class="container">


    <div class="row">

      <div class="col-md-9 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.5s">
        <div class="sec-title">
          <div class="pretitle">Go Digital.</div>
          <div class="title">welcome to <br> dubai manuplicity</div>
        </div>
      </div>


      <div class="col-md-3  wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.5s">
        <div class="logo-block-holder">
          <div class="logo-image-holder">
            <img src="assets/images/main-logo.png">
          </div>
        
        </div>


      </div>

    </div>



  </div>

</section>




<section class="section-padding single-product">
  <div class="container">

    <div class="row single-product-holder">
      <div class="col-md-4">
        <div class="single-product-image-holder">
          <img src="assets/images/dummy-product-2.png" />
        </div>
      </div>
      <div class="col-md-8">
        <div class="single-product-body">

          <div class="single-product-title">Hot Coffees</div>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
          <ul>
            <li>
              <label>
                <input type="radio" name="radio-button" value="css" checked />
                <span>CSS-only fully stylable radio button</span>
              </label>
            </li>



            <li>
              <label>
                <input type="radio" name="radio-button" value="css" checked />
                <span>CSS-only fully stylable radio button</span>
              </label>
            </li>



            <li>
              <label>
                <input type="radio" name="radio-button" value="css" checked />
                <span>CSS-only fully stylable radio button</span>
              </label>
            </li>



            <li>
              <label>
                <input type="radio" name="radio-button" value="css" checked />
                <span>CSS-only fully stylable radio button</span>
              </label>
            </li>


 
          </ul>

          <a href="#" class="btn-main">Select</a>

        </div>
      </div>




    </div>



  </div>

</section>
<!------------ Rehan : Simple Page  END--------------------------------->


<?php require_once('footer.php'); ?>
<?php require_once('header.php'); ?>


<!------------ Rehan : Simple Page START--------------------------------->
<section class="section-padding page">
  <div class="container">

    <div class="row">

      <div class="col-md-8 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.5s">
        <div class="sec-title">
          <div class="pretitle">Go Digital.</div>
          <div class="title">welcome to <br> dubai manuplicity</div>
        </div>
      </div>


      <div class="col-md-4  wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.5s">
        <div class="logo-block-holder">
          <div class="logo-image-holder">
            <img src="assets/images/main-logo.png">
          </div>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus tortor eros, ullamcorper eu ante vitae, bibendum commodo nisl. Morbi finibus lacus ac convallis pulvinar. </p>
        </div>


      </div>

    </div>



    </div>

</section>



<section class="section-padding categroy-sugar-type">
  <div class="container">
      <div class="row">
          <div class="col-12 col-sm-12 col-md-12 col-lg-12">
              <h6 class="sugar-type-title">Select Sugar Type</h6>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-6">
            <div class="sugar-type-img-holder">
                <img src="./assets/images/white_sugar.png" />
                <input type="radio" class="sugar-btn" id="sugar" name="sugar" value="white_sugar" checked />
            </div>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-6">
            <div class="sugar-type-img-holder">
                <img src="./assets/images/brown_sugar.png" />
                <input type="radio" id="sugar" name="sugar" value="brown_sugar" />
            </div>
          </div>
      </div>
  </div>
</section>
<!------------ Rehan : Simple Page  END--------------------------------->


<?php require_once('footer.php'); ?>
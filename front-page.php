<!------------ Rehan : Simple Page START--------------------------------->
<section class="section-padding page">
  <div class="container">


    <div class="row">


      <div class="col-md-9 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.5s">



        <div class="row">


          <div class="col-md-6">
            <div class="lang-btn-holder">

              <a href="javascript:;" class="btn-main-radius-without-bg active">
                English </a>
              <a href="javascript:;" class="btn-main-radius-without-bg"> عربي </a>



            </div>
          </div>
          <div class="col-md-5">

            <div class="no-order-icon">
              <div class="order-bg-icon"></div>
              <div class="order-digits">!</div>
              <span>No Order</span>
            </div>
          </div>

        </div>







        <div class="sec-title">
          <div class="pretitle">Go Digital.</div>
          <div class="title">welcome to <br> dubai manuplicity</div>
        </div>
      </div>


      <div class="col-md-3  wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.5s">
        <div class="logo-block-holder">
          <div class="logo-image-holder">
            <img src="assets/images/main-logo.png">
          </div>
          <p>Learn more about the regulations, guidelines, circulars and publications issued by the various departments of Dubai Municipality </p>
          <a class="btn-main has_icon_bill" href="#"> call office boy </a>
        </div>


      </div>

    </div>



  </div>

</section>








<section class="section-padding product-category-2">
  <div class="container">

    <div class="row product-categories owl-carousel owl-theme owl-loaded owl-drag" >

      <?php for ($i = 0; $i < 4; $i++) { ?>
        <div class="col-md-12 product-category-holder-2">
 
              <img src="assets/images/dummy-product-<?php echo $i; ?>.png" />
          
              <div class="white-bg"></div>
              <div class="product-category-title-2">Hot Coffees <a href="#" class="btn-main"> Select </a></div>
              
            </div>

      <?php } ?>

    </div>



  </div>

</section>


<?php /*


<section class="section-padding product-category">
  <div class="container">

    <div class="row">

      <?php for ($i = 0; $i < 4; $i++) { ?>
        <div class="col-md-3">

          <div class="row product-category-holder">
            <img src="assets/images/dummy-product-<?php echo $i; ?>.png" />
            <div class="col-md-5">
            </div>
            <div class="col-md-7">
              <div class="product-category-body">

                <div class="product-category-title">Hot Coffees</div>


              </div>
            </div>

          </div>



        </div>
      <?php } ?>

    </div>



  </div>

</section>


<section class="section-two home-item-slider section-padding">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12 col-md-12 col-lg-12">
        <h6>
          What would you like?
        </h6>
      </div>
    </div>
    <div class="row">
      <?php
      $item_arr = array(

        array(
          "image" => "water.svg",
          "title" => "Fresh Water",
          "active_class" => "active"
        ),
        array(
          "image" => "coffee.svg",
          "title" => "Coffee",
          "active_class" => ""
        ),
        array(
          "image" => "tea.svg",
          "title" => "Tea",
          "active_class" => ""
        ),
        array(
          "image" => "juice.svg",
          "title" => "Fresh Juice",
          "active_class" => ""
        ),
        // array(
        //   "image" => "soft-drink.svg",
        //   "title" => "Soft Drink",
        //   "active_class" => ""
        // ),

      );
      // echo '<pre>'; print_r($item_arr);
      $count_slider_item = 0;
      for ($count_slider_item; $count_slider_item < count($item_arr); $count_slider_item++) {
        // echo $item_arr[$count_slider_item]['title'];
      ?>
        <div class="col-12 col-sm-12 col-md-3 col-lg-3">
          <div class="horizental-container">
            <div class="slider-boxs">
              <div class="item-image">
                <img src="./assets/images/svg/<?php echo $item_arr[$count_slider_item]['image']; ?>" />
              </div>
              <div class="box-item-name">
                <h3><?php echo $item_arr[$count_slider_item]['title']; ?></h3>
              </div>
              <div class="box-select-item-btn <?php echo $item_arr[$count_slider_item]['active_class']; ?> default-color-full-btn">
                <a href="javascript:;" class="select-btn default-txt-color">
                  Select
                </a>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
      <?php /* 
      
       <div class="row">
      <div class="col-12 col-sm-12 col-md-12 col-lg-12">
        <div class="home-item-box-horizental">
                <div class="item">
                  <div class="home-item-img-holder">
                    <img src="./assets/images/svg/tea.svg<?php //echo $item_arr[$count_slider_item]['image']; ?>" />
                  </div>
                  <div class="home-item-title">
                    <h3>tea<?php //echo $item_arr[$count_slider_item]['title']; ?></h3>
                  </div>
                </div>  
                <div class="item">
                  <div class="home-item-img-holder">
                    <img src="./assets/images/svg/water.svg<?php //echo $item_arr[$count_slider_item]['image']; ?>" />
                  </div>
                  <div class="home-item-title">
                    <h3>tea<?php //echo $item_arr[$count_slider_item]['title']; ?></h3>
                  </div>
                </div>  
        </div>
      </div>
    </div>

      
    </div>



  </div>
</section> */ ?>
<?php require_once('config.php');?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Hosptality </title>


<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


<!-- FAV Icons -->
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $base_url; ?>assets/images/fav/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $base_url; ?>assets/images/fav/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $base_url; ?>assets/images/fav/favicon-16x16.png">
<link rel="manifest" href="<?php echo $base_url; ?>assets/images/fav/site.webmanifest">
<link rel="mask-icon" href="<?php echo $base_url; ?>assets/images/fav/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="theme-color" content="#ffffff">



<!-- Bootstrap CSS -->
<link type="" rel="stylesheet" href="<?php echo $base_url; ?>assets/css/bootstrap.min.css" crossorigin="anonymous">
<link href="<?php echo $base_url; ?>assets/css/style.css" rel="stylesheet">
 
</head>
<body>
<div class="main" >

<div class="preloader">
  <div class="svg_image"></div>
</div>  




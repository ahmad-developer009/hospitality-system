<?php require_once('header.php'); ?>


<!------------ Rehan : Simple Page START--------------------------------->
<section class="section-padding-small">
  <div class="container">

    <div class="row">

      <div class="col-md-9 wow fadeInLeft" data-wow-duration="2s" data-wow-delay="0.5s">
        <div class="sec-title">

        </div>
      </div>

      <div class="col-md-3  wow fadeInRight" data-wow-duration="2s" data-wow-delay="0.5s">
        <div class="logo-block-holder">
          <div class="logo-image-holder">
            <img src="assets/images/main-logo.png">
          </div>
        
        </div>


      </div>

    </div>

  </div>

</section>




<section class="section-padding-small">
  <div class="container">

  <!--Message Block-->
    <div class="row">
      <div class="col-md-12">
        <div class="message">
          <div class="message-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </div>
          <a href="#" class="btn-main">Go to home</a>
        </div>

      </div>
    </div>


<!--Maxima Block-->
    <div class="row">
      <div class="col-md-12">
        <div class="maximablock">
        <div class="qr-code-holder"> <img src="assets/images/qr.png"></div>
            
        <p class="text-holder">Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
        <div class="logo-holder"> <img src="assets/images/maxima.png"> </div>
          
        </div>

      </div>
    </div>



  </div>

</section>
<!------------ Rehan : Simple Page  END--------------------------------->


<?php require_once('footer.php'); ?>
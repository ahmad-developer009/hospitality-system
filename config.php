<?php

$base_url = 'http://localhost:8888/hospitality-system/';
$resturent_name = 'Website Name';



$menu = [	
 'home' => '',
 'about-us' => 'about.php',
 'contact-us' => 'contact.php',
 'menu' => 'menu.php',
 'blog' => 'blog.php',
 'reservation' => 'reservation.php',
];


$custom_text = [	
 'header-logo' => 'assets/images/logo.png',
 'footer-logo' => 'assets/images/footer-logo.png',
 'menu-logo' => 'assets/images/menu-logo.png',
 'contact-number' => '+971 04 9883 898',
 'fax-number' => '+971 04 9883 898',
 'email-address' => 'info@website.com',
 'address' => '49 Featherstone Street, LONDON, EC1Y 8SY, UNITED KINGDOM',
 'address_google_url' => '',
 'footer-text'=> 'Chef Taylor Bonnyman, working in collaboration with Head Chef Marguerite Keogh, offer elegant & playful modern British cooking.',
];



$social_links = [
	[
	'social_link_name' => 'facebook',
	'social_link_url' => '#',
	'social_link_class' => 'fa fa-facebook',
	],
	[
	'social_link_name' => 'twitter',
	'social_link_url' => '#',
	'social_link_class' => 'fa fa-twitter',
	],
	[
	'social_link_name' => 'instagram',
	'social_link_url' => '#',
	'social_link_class' => 'fa fa-instagram',
	],
	[
	'social_link_name' => 'youtube',
	'social_link_url' => '#',
	'social_link_class' => 'fa fa-youtube',
	],
];




$food_gallery_tabs = [
	[
	'title' => 'all',
	'catergory_class' => 'all',
	'active' => '',
	],
];

$food_gallery = [
	[
	'title' => '',
	'price' => '',
	'catergory_class' => 'main',
	'thumb_image' => 'assets/images/product-img1.jpg',
	'larg_image' => 'assets/images/slider-2.jpg',
	],
];




$food_menu_list = [
	
	[
	'title' => '',
	'price' => '',
	'catergory_class' => 'all',
	'thumb_image' => 'assets/images/product-img3.jpg',
	'larg_image' => 'assets/images/product-img3.jpg',
	],
	[
	'title' => '',
	'price' => '',
	'catergory_class' => 'all',
	'thumb_image' => 'assets/images/product-img4.jpg',
	'larg_image' => 'assets/images/product-img4.jpg',
	],
];


?>

<footer class="main-footer wow fadeInUp"  data-wow-duration="2s" data-wow-delay="0.5s">
 

 
 
    <div class="container">
    <div class="row copyright">
      <div class="col-md-12">
        <p> &copy; <?php echo date('Y');?> All Rights Reserved. Website by <a href="https://maximagroup.ae" target="_blank">Maximagroup</a>.
      </div>

    </div>

  </div>

 


</footer>

</div>
<!-- 
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-up"></span></div>
 -->

<script src="assets/js/jquery-3.5.1.min.js" ></script>

<script src="assets/js/owl.carousel.min.js"></script> 

  <script src="assets/js/appear.js"></script> 
<script src="assets/js/wow.js"></script> 
 
<script src="assets/js/jquery.fancybox.pack.js"></script> 
<script src="assets/js/jquery.fancybox-media.js"></script>  
<script src="assets/js/script.js"></script> 

</body>

</html>